<%--
  Created by IntelliJ IDEA.
  User: uzivatel
  Date: 25. 10. 2020
  Time: 16:57
  To change this template use File | Settings | File Templates.
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>Student Confirmation</title>
</head>
<body>

<p>Student of confirmed: ${student.firstName} ${student.lastName}</p>
<br><br>

<p>Country: ${student.country}</p>
<br><br>

<p>Favorite Language: ${student.favoriteLanguage}</p>
<br>

Operating Systems:
<ul>
    <c:forEach var="temp" items="${student.operatingSystems}">
        <li> ${temp} </li>
    </c:forEach>
</ul>



</body>
</html>
