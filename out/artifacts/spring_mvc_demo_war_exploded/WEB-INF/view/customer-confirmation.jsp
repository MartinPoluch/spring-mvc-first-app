<%--
  Created by IntelliJ IDEA.
  User: uzivatel
  Date: 27. 10. 2020
  Time: 22:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>Customer Confirmation</title>
</head>
<body>

The customer is confirmed: ${customer.firstName} ${customer.lastName}
<br>
<p>Free passes: ${customer.freePasses}</p>
<br>
<p>Postal code: ${customer.postalCode}</p>
<br>
<p>Course code: ${customer.courseCode}</p>

</body>
</html>
