<%--
  Created by IntelliJ IDEA.
  User: uzivatel
  Date: 25. 10. 2020
  Time: 16:47
  To change this template use File | Settings | File Templates.
--%>
<%--<%@ page contentType="text/html;charset=UTF-8" language="java" %>--%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="foem" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Title</title>
</head>
<body>

<form:form action="processForm" modelAttribute="student">

    First name: <form:input path="firstName"/>
    <br> <br>

    Last name: <form:input path="lastName"/>
    <br> <br>

    Country:
    <form:select path="country">
        <form:options items="${student.countryOptions}"/>
    </form:select>
    <br> <br>

    Favorite Language:
    <form:radiobuttons path="favoriteLanguage" items="${student.favoriteLanguageOptions}"/>

    <br> <br>
    Operating Systems:
    Linux <form:checkbox path="operatingSystems" value="Linux"/>
    Mac <form:checkbox path="operatingSystems" value="Mac"/>
    Windows <form:checkbox path="operatingSystems" value="Windows"/>
    <br> <br>

    <input type="submit" value="Submit">

</form:form>

</body>
</html>
