package com.luvcode.springdemo.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/hello") // high level mapping => prefix for all method RequestMapping
public class HelloWordController {

    // need a controller method to show initial HTML form
    @RequestMapping("/showForm")
    public String showForm() {
        return "helloword-form";
    }


    // method to process the HTML form
    @RequestMapping("/processForm")
    public String processForm() {
        return "helloworld";
    }

    /**
     * Method to read form data and add data to the model
     * @param request values which send to controller.
     * @param model values which will be sent into view.
     * @return name of view file
     */
    @RequestMapping("/processFormVersionTwo")
    public String letsShoutDude(HttpServletRequest request, Model model) {
        String name = request.getParameter("studentName");
        name = name.toUpperCase();
        String bigName = "Yo! " + name;
        model.addAttribute("message", bigName);
        return "helloworld";
    }

    /**
     * Method to read form data and add data to the model
     * @param name name from form is bind to variable name by RequestParam annotation => request.getParameter("studentName")
     * @param model
     * @return name of view file
     */
    @RequestMapping("/processFormVersionThree")
    public String processFormVersionThree(@RequestParam("studentName") String name, Model model) {
        name = name.toUpperCase();
        String bigName = "Hey my friend from v3! " + name;
        model.addAttribute("message", bigName);
        return "helloworld";
    }
}
