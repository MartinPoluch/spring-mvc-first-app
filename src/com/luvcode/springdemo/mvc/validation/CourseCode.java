package com.luvcode.springdemo.mvc.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = CourseCodeConstraintValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface CourseCode {

    String DEFAULT_VALUE = "LUV";

    /**
     * parameter of annotation
     */
    String value() default DEFAULT_VALUE;

    /**
     * parameter of annotation
     */
    String message() default "must start with " + DEFAULT_VALUE;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
